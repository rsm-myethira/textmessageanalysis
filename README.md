# Text Analysis

Relationships. 

In today's world, it is hard to find people you have a meaningful connection with. Sometimes, you find people you have for life. Other times though, you meet people who are only there for a little while.
I'm an idealist. I believe in the idea of growing up together and having long-lasting relationships. Being able to say that you have known someone closely for 40 years is the kind of thing I aspire to say when I'm old. 

Unfortunately, as you grow older, you begin to realize that it is not easy to form relationships that last. Life happens, goals change, people change, and the things you have in common are no longer enough to keep you together. It hurts when you have to say goodbye to a relationship that meant so much to you. 
By using text messages exchanged between two people, I'd like to find out if there are any patterns that would help determine if a relationship will last or not. 

For highlighting the differences in patterns, I used text messages exchanged between me and six of my friends. This is because, the number of text messages exchanged in a short-term relationship is significantly higher than that in a long-term relationship. So, to balance this, I used text messages exchanged with more people that I have a long-term relationship with. 


# Data Collection

I used text messaged sent using Facebook Messenger and WhatsApp. To download messages sent using Facebook, follow the steps on this[link](https://www.zapptales.com/en/download-facebook-messenger-chat-history-how-to/) link. 
To download messages sent via WhatsApp, follow the steps for exporting chats on this [link](https://faq.whatsapp.com/en/wp/22548236). 


# Data Cleaning

The code to clean messages sent using Messenger is available in the Script.R file. 
To clean WhatsApp messages, follow the instruction on this [link](https://journocode.com/2016/01/31/project-visualizing-whatsapp-chat-logs-part-1-cleaning-data/)

# Results

From our analysis, we can infer that fewer text messages are exchanged in long-term friendships. This could either be because the conversations mostly happen over phone, or because you are in touch less often. 
There are a lot of ideas out there about how one should form and maintain long-lasting relationships. Data seems to support the idea that the longevity of a relationship is determined by ability of the people involved to give each other the space to grow independently and make their own decisions. 
